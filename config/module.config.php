<?php

return array(

    'controllers' => include 'controller.config.php',

    'router' => include 'router.config.php',

    'service_manager' => include 'service.config.php',

    'view_manager' => array(
        // 'strategies' => array(
        //     'ViewJsonStrategy',
        // ),
        'template_map' => array(
            'layout/cms-install-layout' => __DIR__ . '/../view/layout/cms-install-layout.twig',
            'cms/install/index'         => __DIR__ . '/../view/cms-install/install.twig'
        )
    ),

    // 'module_layouts' => array(
    //     'Cms\Install'   => 'layout/cms-install-layout',
    // ),

    'bas_cms' => array(
        'extensions' => array(
            'install-manager' => array(
                'type'    => 'Cms\Client\Install\Extension\InstallManager',
                'options' => array(
                    'listeners' => array(
                        'installation.request'   => 'installationRequester',
                    )
                ),
            )
        )
    )
);