<?php 

return array(
    'routes' => array(
        'cms-installation' => array(
            'type' => 'literal',
            'options' => array(
                'route'    => '/install',
                'defaults' => array(
                    'controller' => 'bas.cms.controller.install',
                    'action'     => 'index',
                ),
            ),
    
        ),
    ),
);