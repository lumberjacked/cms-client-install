<?php
namespace Cms\Client\Install\Controller;

use Zend\Http\Client;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;

class InstallController extends AbstractActionController {
    
    public function indexAction() {
    	
        $xmanager = $this->plugin('cms.extension.plugin')->getXmanager();
        
        if($this->getRequest()->isPost()) {

        	$data = $this->getRequest()->getPost();
            

            $responder = $xmanager->client('installation.request', 'post', 'installation', $data->toArray());
            
            return $this->redirect()->toRoute('cms-admin');


        } else if(!$xmanager->trigger('get.cms.config')->isInstalled()) {
            
            return array('data' => array('template' => 'installation-wizard', 'submit' => 'installation'));

        } else {  
            return $this->redirect()->toRoute('cms-login');
        }
    }
} 