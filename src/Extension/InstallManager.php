<?php
namespace Cms\Client\Install\Extension;

use Zend\Json\Json;
use Zend\Http\Client;
use Zend\Config\Config;
use Zend\Stdlib\Parameters;
use Zend\Validator\File\Exists;
use Doctrine\ORM\Tools\ToolsException;
use Cms\ExtensionManager\Extension\Requester;
use Cms\ExtensionManager\Extension\ResponderEvent;
use Cms\ExtensionManager\Extension\AbstractExtension;

class InstallManager extends AbstractExtension {

    const INSTALLTION = 'installation.event';

    protected $defaultAdmin = array();

    public function installationRequester(ResponderEvent $e) {
        
        if(!($e->getParams() instanceof Requester)) {
            return $e->responder(null, true, 
                     sprintf("Client calls require the use of Cms/ExtensionManager/Extension/Requester -- %s given instead", 
                         gettype($e->getParams())
                    ),
                    array('received' => $e->getParams()),
                    500
            );
        }
        
        $requester = $e->getParams();

        //TODO:  move defining client credentials to defaultAdmin
        //TODO:: there are other problems with this that needs to be addressed
        $login_user = array(
                'grant_type' => 'client_credentials',
                'email'      => $requester->getParam('email'),
                'password'   => $requester->getParam('password'),
                'headers' => array(
                    'auth' => array(
                        'email'      => $requester->getParam('email'),
                        'password'   => $requester->getParam('password'),
                    )
                )  
            );

        $responder = $this->trigger('api', $requester);
        if($responder->isError()) {
            return $responder;
        }
        
        $data = $responder->getData();

        $responder = $this->trigger('cms.config.create', $data['servers']);
        if($responder->isError()) {
            return $responder;
        }
        
        $requester = $requester->newRequester('login', 'post', $login_user);

        $responder = $this->trigger('login.request', $requester);
        if($responder->isError()) {
            return $responder;
        }

        $config = $this->trigger('get.cms.config');
        $config = $config->getConfig()->toArray();
        return $e->responder(null, false, 'Succesfully Completed Installation', array('local_config' => $config, 'identity' => $responder->getData()), 200);
        
    }

    public function installationEvent(ResponderEvent $e) {
        
        $params   = $e->getParams();
    
        if(is_object($params)) {
            $params = get_object_vars($params);
            $params = new Parameters($params);
        }
        $params = $this->extractDefaultAdmin($params);
        
        $triggers = array('cms.config.create', 'cms.schema.create');
        
        foreach($triggers as $index => $trigger) {
            $responder = $this->trigger($trigger, $params->toArray());
            if($responder->isError()) {
                return $responder;
            }
        }
        
        $responder = $this->trigger('cms.auth.create.user', $this->defaultAdmin);
        
        if($responder->isError()) {
            return $responder;
        }

        $config = $this->trigger('get.cms.config')->getConfig();
        $server = $config->bas_cms->config->get('servers');
        return $e->responder(false, 'Succesfully Completed Installation', array('servers' => $server->toArray(), 'user' => $responder->getData()), 200, true);
    }

    // public function installationAuth(ResponderEvent $e) {
    //     $params = $e->getParams();
        
        
    //     $uri = $uri . '/login';

    //     if(!array_key_exists('email', $params) || !array_key_exists('password', $params)) {
    //         return $e->responder(true, 'Must provide email & password for authentication!');
    //     }

    //     $client = new Client($uri);
    //     $client->setAdapter('Zend\Http\Client\Adapter\Curl');
    //     $client->setMethod('post');
    //     $client->setParameterPost(array('grant_type' => 'client_credentials'));
    //     $client->setAuth($params['email'], $params['password']);
        
    //     $headers = $client->getRequest()->getHeaders();
    //     $headers->addHeaderLine('Accept', 'application/json');
        
    //     $response = $client->send();
    //     if(!$response->isSuccess()) {
    //         $responder = Json::decode($response->getContent(), Json::TYPE_ARRAY);
        
    //         return $e->responder(true, $responder['detail'], null, $responder['status']);

    //     }

    //     $responder = Json::decode($response->getContent(), Json::TYPE_ARRAY);
        
    //     return $e->responder(false, sprintf('Successfully logged in -- token type %s', $responder['token_type']), array('access_token' => $responder['access_token']));
    // }

    protected function getDefaultCredentials() {
        $params['email']    = $this->defaultAdmin['email'];
        $params['password'] = $this->defaultAdmin['password'];

        return $params;
    }

    protected function extractDefaultAdmin(array $params) {
        
        $params = new Parameters($params);

        $this->defaultAdmin = array(
                'roles'            => $params->get('userrole', 'superman'),
                'email'            => $params->get('email', 'admin@admin.com'),
                'password'         => $params->get('password', 'Reset this password!'),
                'password_confirm' => $params->get('password_confirm', 'Reset this password!')
        );

        foreach(array('email', 'password', 'password_confirm') as $index => $key) {
            if(array_key_exists($key, $params)) {
                unset($params[$key]);
            }
        }
        
        return $params;
    }
}